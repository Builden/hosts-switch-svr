import fs from 'fs-extra';
import _ from 'lodash';
import moment from 'moment';
import logger from '../utils/bunyan-logger';
const console = logger('rules');
let predefRules = null;

const bakFile = './res/predefRules.json';

function nowStr() {
  return moment().format('YYYY-MM-DD HH:mm:ss');
}

function load() {
  if (fs.existsSync(bakFile)) {
    predefRules = fs.readJsonSync(bakFile);
    console.log('loaded', predefRules);
  }

  if (!predefRules) predefRules = [];
}

load();

function save() {
  fs.writeJsonSync(bakFile, predefRules);
}

export async function get(body) {
  return {
    ret: 0,
    predefRules,
  };
}

export async function post(body) {
  const {name, code} = body;
  if (!name || !code) return { ret: -2 };
  let one = _.find(predefRules, { name });
  if (one) {
    if (one.code !== code) {
      one.code = code;
      one.updateTime = nowStr();
      one.ver += 1;
      save();
    }
    console.log('update rule', body);
  } else {
    one = {
      name,
      type: 'predef',
      code,
      ver: 0,
      updateTime: nowStr(),
    };
    predefRules.push(one);
    console.log('add rule', body);
    save();
  }
  return { ret: 0, rule: one };
}

export async function del(body) {
  const {name} = body;
  const removed = _.remove(predefRules, (one) => {
    return (one.name === name);
  });
  if(removed.length > 0) save();
  return { ret: 0 };
}
