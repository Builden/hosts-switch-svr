import express from 'express';
import logger from './utils/bunyan-logger';
const console = logger('api-router');
const router = express.Router();
const rules = require('./api/rules');
const rulesPath = '/rules';

router.get(rulesPath, async (req, res) => {
  try {
    const rst = await rules.get(req.query);
    console.log('get rst', rst);
    res.json(rst);
  } catch (e) {
    res.json({ ret: -1, err: e });
  }
});

router.post(rulesPath, async (req, res) => {
  try {
    const rst = await rules.post(req.body);
    console.log('post rst', rst);
    res.json(rst);
  } catch (e) {
    res.json({ ret: -1, err: e, stack: e.stack });
  }
});

router.delete(rulesPath, async (req, res) => {
  try {
    const rst = await rules.del(req.body);
    res.json(rst);
  } catch (e) {
    res.json({ ret: -1, err: e });
  }
});


module.exports = router;
