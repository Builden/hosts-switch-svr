import compression from 'compression';
import favicon from 'serve-favicon';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import morgan from 'morgan';
import errorHandler from 'errorhandler';
import cors from 'cors';

export default function expressLoadPlugins(app) {
  const env = app.get('env');

  app.use(cors());
  app.use(favicon('res/favicon.ico'));
  app.use(compression());
  app.use(bodyParser.urlencoded({
    extended: false,
  }));
  app.use(bodyParser.json());
  app.use(methodOverride());

  if (env === 'development') {
    app.use(morgan('dev'));
    app.use(errorHandler());
  }
}
