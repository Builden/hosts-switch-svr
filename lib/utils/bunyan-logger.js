import bunyan from 'bunyan';
import fs from 'fs-extra';

const logDir = __dirname + '/../../logs/';
fs.ensureDirSync(logDir);

export default function logger(name) {
  const l =  bunyan.createLogger({
    name,
    src: (process.env.NODE_ENV !== 'production'),
    streams: [
      {
        level: 'debug',
        stream: process.stdout,
      },
      {
        level: 'info',
        type: 'rotating-file',
        path: logDir + 'info.log',
        period: '1d',
        count: 5,
      },
    ],
  });
  l.log = l.info;
  return l;
}
