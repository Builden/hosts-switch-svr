import 'babel-polyfill';
import argv from 'commander';
import fs from 'fs-extra';
import express from 'express';
import logger from './utils/bunyan-logger';

const console = logger('app');
const pkginfo = fs.readJsonSync(__dirname + '/../package.json');

argv
  .version(pkginfo.version)
  .option('-p, --port <n>', 'listen port', parseInt)
  .parse(process.argv);

argv.port = argv.port || 3001;


process.on('uncaughtException', (e) => {
  console.fatal('process Caught exception: ' + e.stack);
});

import expressLoadPlugins from './utils/express-load-plugins';
const app = express();
expressLoadPlugins(app);

app.use('/api', require('./api-router'));
app.listen(argv.port, (err) => {
  if (err) return console.error('listen on ', argv.port, 'failed.', err);
  return console.info('listen on', argv.port);
});
